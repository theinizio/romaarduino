import api_routes from '~/store/api-routes';
import fetchData from "~/js/fetchData";

export default {
    actions: {
        getUsers({dispatch, commit}, page) {
            return fetchData('get', `${api_routes.state.api_routes.user}?page=${page}`);
        },
        getUser({dispatch, commit}, slug) {
            return fetchData('get', `${api_routes.state.api_routes.users}/${slug}/edit`);
        },
        createUser({dispatch, commit}, data) {
            return fetchData('post', `${api_routes.state.api_routes.users}`, data);
        },
        editUser({dispatch, commit}, {data, id}) {
            return fetchData('post', `${api_routes.state.api_routes.users}/${id}`, data);
        },
        deleteUser({dispatch, commit}, id) {
            return fetchData('delete', `${api_routes.state.api_routes.users}/${id}`);
        },
        searchUsers({}, search) {
            return fetchData('get', `${api_routes.state.api_routes.users}${search}`);
        },
    }
}

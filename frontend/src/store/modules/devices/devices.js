import api_routes from '../../api-routes';
import fetchData from "../../../js/fetchData";

export default {
    actions: {
        // Devices
        async getDevices({dispatch, commit}, page) {
            const result = await fetchData('get', `${api_routes.state.api_routes.device}?page=${page}`);
            console.log(result)
            return result
        },
        async getDeviceData({dispatch, commit}, id) {
            console.log(api_routes.state.api_routes.lastDeviceData);
            const result = await fetchData('get', `${api_routes.state.api_routes.lastDeviceData}/${id}`);
            console.log(result)
            return result
        },
        async getDeviceChartData({dispatch, commit}, id) {
            console.log(api_routes.state.api_routes.deviceChartData);
            const result = await fetchData('get', `${api_routes.state.api_routes.deviceChartData}/${id}`);
            console.log(result)
            return result
        },
        async sendModeToDevice({dispatch, commit}, data) {
            console.log(api_routes.state.api_routes.send_mode_to_device);
            const result = await fetchData('post', api_routes.state.api_routes.send_mode_to_device, data);
            console.log(result)
            return result
        },
        async getTimetable({dispatch, commit}, id) {
            console.log(api_routes.state.api_routes.timetable);
            const result = await fetchData('get', `${api_routes.state.api_routes.timetable}/${id}`);
            console.log(result)
            return result
        },
        async deleteTimetable({dispatch, commit}, id) {
            console.log(api_routes.state.api_routes.timetable);
            const result = await fetchData('delete', `${api_routes.state.api_routes.timetable}/${id}`);
            console.log(result)
            return result
        },
        async addTimetable({dispatch, commit}, data) {
            console.log(api_routes.state.api_routes.timetable);
            const result = await fetchData('post', `${api_routes.state.api_routes.timetable}`, data);
            console.log(result)
            return result
        },
    },
}

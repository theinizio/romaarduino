import fetchData from "~/js/fetchData";
import api_routes from '../../api-routes';

export default {
    actions: {
        getAlbum({}, page) {
            return fetchData('get', `${api_routes.state.api_routes.media}?page=${page}`);
        },
        createNewAlbum({dispatch, commit}, data) {
            return fetchData('post', `${api_routes.state.api_routes.media}`, data);
        },
        saveAlbum({dispatch, commit}, {id, album}) {
            return fetchData('post', `${api_routes.state.api_routes.media}/${id}`, album);
        },
        deleteAlbum({dispatch, commit}, id) {
            return fetchData('delete', `${api_routes.state.api_routes.media}/${id}`);
        },
        deletePhoto({dispatch, commit}, {albumId, photoId}) {
            return fetchData('delete', `${api_routes.state.api_routes.media}/${albumId}/delete_picture/${photoId}`);
        }
    }
}

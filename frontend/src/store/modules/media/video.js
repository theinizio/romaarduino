import fetchData from "~/js/fetchData";
import api_routes from '../../api-routes';

export default {
    actions: {
        getVideoAlbum({}, page) {
            return fetchData('get', `${api_routes.state.api_routes.mediaVideo}?page=${page}`);
        },
        createNewVideoAlbum({dispatch, commit}, data) {
            return fetchData('post', `${api_routes.state.api_routes.media}`, data);
        },
        saveVideoAlbum({dispatch, commit}, {id, album}) {
            return fetchData('post', `${api_routes.state.api_routes.media}/${id}`, album);
        },
        deleteVideoAlbum({dispatch, commit}, id) {
            return fetchData('delete', `${api_routes.state.api_routes.media}/${id}`);
        }
    }
}

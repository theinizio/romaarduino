import api_routes from '../../api-routes';
import fetchData from "../../../js/fetchData";

export default {
    actions: {
        // staticPages
        async getStaticPages({dispatch, commit}) {
            const result = await fetchData('get', `${api_routes.state.api_routes.staticPages}`)
            console.log(result)
            return result.data
        },
        async getStaticPage({dispatch, commit}, pageSlug) {
            const result = await fetchData('get', `${api_routes.state.api_routes.staticPages}/${pageSlug}`)
            console.log(result)
            return result
        },
        async saveAllStaticPages({dispatch, commit}, data) {
            const result = await fetchData(
                'post',
                `${api_routes.state.api_routes.staticPages}/save_all`,
                {pages:data}
                )
            console.log(result)
            return result
        },
    }
}
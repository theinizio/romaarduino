import Vue from 'vue';
import Vuex from 'vuex';
import apiRoutes from './api-routes';
import devices from "./modules/devices/devices";
import users from "./modules/users/users";
import staticPages from "./modules/staticPages/staticPages";
import fetchData from "../js/fetchData";
import api_routes from "./api-routes";
import router from "../router/router";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        token: localStorage.getItem('token') || '',
        user: JSON.parse(localStorage.getItem('user')) || {},
        userRole: localStorage.getItem('user')?JSON.parse(localStorage.getItem('user')).role : 'user',
        staticPages: [],
    },
    actions: {
        async login({dispatch, commit}, { email, password }) {

            const result = await fetchData('post', `${api_routes.state.api_routes.login}`, { email, password });
            console.log(result);
            if (result.status === 200) {
                this.error = false;
                commit('setToken', result.data.token);
                commit('setUser', result.data.$attributes);
                return true;
            } else {
                return false;
            }
        },
        async logout({dispatch, commit}) {
            try {
                console.log('in logout');
                const res = await fetchData('post', api_routes.state.api_routes.logout);
                console.log(res);
                commit('setToken', null);
                commit('setUser', null);
                // delete this.axios.defaults.headers;
                router.push({name: 'login'});
            }catch (e) {
                console.log(e)
            }
        },
        checkLocalStorage({dispatch, commit}) {
            const token = localStorage.getItem('token');
            const user = JSON.parse(localStorage.getItem('user'));

            if (token){ commit('setToken', token);}
            if (user) {commit('setUser', user);}
        },
        async setRelation({dispatch, commit}, data) {
            console.log(data);
            const res = await fetchData('post', api_routes.state.api_routes.set_device_owner, data);
            console.log(res);
            return res.status === 200;

        },
    },
    getters: {
        getToken: state => {
            return state.token || localStorage.getItem('token');
        },
        getUser: state => state.user || JSON.parse(localStorage.getItem('user')),
        getUserRole: state => state.userRole || JSON.parse(localStorage.getItem('user')).role,
        isAdmin: state => state.userRole === 'admin',
        staticPages: state => state.staticPages,
    },

    mutations: {
        setToken: (state, payload) => {
            state.token = payload;
            if(payload) {
                localStorage.setItem('token', payload);
            }else{
                localStorage.removeItem('token');
            }

        },
        setUser: (state, payload) => {

            if(payload !== null) {
                state.user = {...payload};
                state.userRole = payload.role;
                localStorage.setItem('user', JSON.stringify(payload));
            }else {
                localStorage.removeItem('user');
            }
            console.log(state);
        },
        setStaticPages: ((state, payload) => state.staticPages = payload),

    },

    modules: {
        apiRoutes,
        devices,
        users,
        staticPages,
    },
})

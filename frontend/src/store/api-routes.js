export default {
    state: {
        api_routes: {
            login: '/api/login',
            logout: '/api/logout',
            changePassword: '/api/change_password',
            resetPassword: '/api/reset_password',
            register: '/api/register',
            socialRegister: '/api/socialRegister',

            user: '/api/admin/user',
            relations: '/api/admin/relations',
            stats: '/api/admin/stats',

            timetable: '/api/timetable',
            profile: '/api/profile',
            device: '/api/device',
            staticPages: '/api/static_page',
            errors: '/api/errors',
            lastDeviceData: '/api/get_data',
            deviceChartData: '/api/device_chart_data',
            send_mode_to_device: '/api/send_mode_to_device',
            send_timetable_to_device: '/api/send_timetable_to_device',
            set_device_owner: '/api/set_device_owner',
        }
    },
    actions: {
        sayHello(){
            consoel.log('Hello')
        }
    }
}

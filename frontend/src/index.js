import 'core-js';
import 'regenerator-runtime/runtime';
import Vue from 'vue';
import App from './components/App.vue';
import router from './router/router.js';
import store from './store';
import routerConfig from './router/config.js';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import axios from 'axios';
import VueAxios from 'vue-axios';
import { VueEditor } from "vue2-editor";
import Loader from './components/Loader/Loader.vue';
import LoaderFixed from './components/Loader/LoaderFixed.vue';
import Paginate from 'vuejs-paginate';
import dateFilter from './js/dateFilter';

routerConfig();

window.axios = require('axios');

const moment = require('moment')
require('moment/locale/ru')

Vue.use(require('vue-moment'), {
  moment
})

console.log(Vue.moment().locale()) //es
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueAxios, axios);

Vue.filter('dateFilter', dateFilter);

Vue.component('VueEditor', VueEditor);
Vue.component('Loader', Loader);
Vue.component('LoaderFixed', LoaderFixed);
Vue.component('Paginate', Paginate);


// let token = document.head.querySelector('meta[name="csrf-token"]');

// if (token) {
//   window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
// } else {
//   console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
// }


const app = new Vue({
  store,
  router,
  axios,
  render: h => h(App),
}).$mount('#app');

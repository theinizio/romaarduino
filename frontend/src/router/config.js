import router from "./router.js";
import store from "../store";

export default () => {
     router.beforeEach(async (to, from, next) => {
console.dir (to.name, to.fullPath)
        if (to.name === 'login') {
            next();
            return;
        }

        const token = store.getters.getToken;
        console.log('token',token);
        if (!token || parseJwt(token).exp < Date.now() / 1000) {
            // console.log(123, await store.dispatch('setToken', null))

            // next(false);
            router.push('/login');
        }

        if (store.getters.isAdmin || to.fullPath.indexOf('admin') === -1) {

            next();
        } else {
            router.push(to.fullPath.replace('/admin', ''))

        }
        if(!Array.from(store.getters.staticPages).length){
           store.commit('setStaticPages', await store.dispatch('getStaticPages'))
        }
    });
}

function parseJwt(token) {
    let base64Url = token.split('.')[1];
    let base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    let jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
}

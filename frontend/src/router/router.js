import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

export default new VueRouter({
    // mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        // no coincidences
        { path: '*', component: () => import('../components/NotFoundComponent.vue') },

        {
            path: '/',
            redirect: '/admin/devices',
        },

        {
            path: '/login',
            name: 'login',
            meta: {layout: 'auth'},
            component: () => import('../components/Auth/Login.vue'),
        },

        /* Admin panel */

        // Devices
        {
            path: '/admin/devices',
            name: 'devices',
            meta: {layout: 'admin'},
            component: () => import('../components/Devices/Devices.vue'),
        },
        {
            path: '/devices/set_device_owner',
            name: 'set_device_owner',
            meta: {layout: 'user'},
            component: () => import('../components/Devices/SetRelation.vue'),
        },
        //One device
        {
            path: '/admin/devices/:id',
            name: 'controlDeviceAdmin',
            meta: {layout: 'admin'},
            component: () => import('../components/Devices/Device.vue'),
        },
        {
            path: '/devices/:id',
            name: 'controlDeviceUser',
            meta: {layout: 'user'},
            component: () => import('../components/Devices/Device.vue'),
        },
        {
            path: '/admin/devices/timetable/:id',
            name: 'timetable',
            meta: {layout: 'admin'},
            component: () => import('../components/Devices/Timetable.vue'),
        },
        {
            path: '/devices/timetable/:id',
            name: 'timetable',
            meta: {layout: 'user'},
            component: () => import('../components/Devices/Timetable.vue'),
        },

        // Users All
        {
            path: '/admin/users',
            name: 'users',
            meta: {layout: 'admin'},
            component: () => import('../components/Users/UsersAll/UsersAll.vue'),
        },
        {
            path: '/admin/users/create',
            name: 'usersCreate',
            meta: {layout: 'admin'},
            component: () => import('../components/Users/UsersAll/UsersCreate.vue'),
        },
        {
            path: '/admin/users/edit/:id',
            name: 'usersEdit',
            meta: {layout: 'admin'},
            component: () => import('../components/Users/UsersAll/UsersEdit.vue'),
        },
        {
            path: '/admin/pages',
            name: 'pages',
            meta: {layout: 'admin'},
            component: () => import('../components/StaticPage/Editor.vue'),
        },
        {
            path: '/devices',
            name: 'user_devices',
            meta: {layout: 'user'},
            component: () => import('../components/Devices/Devices.vue'),
        },
        {
            path: '/pages/:slug',
            name: 'static_page',
            meta: {layout: 'user'},
            component: () => import('../components/StaticPage/StaticPage.vue'),
        },

    ]
})

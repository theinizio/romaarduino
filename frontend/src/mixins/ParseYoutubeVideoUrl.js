export default {
    methods: {
        parseVideoUrl(path) {
            if (path.length < 34) {
                return `https://www.youtube.com/embed/${path.slice(17, 29)}`
            } else if (path.length < 43 && path.length > 33) {
                return path;
            } else {
                return `https://www.youtube.com/embed/${path.slice(32, 43)}`
            }
        }
    }
}

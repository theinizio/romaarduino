export default {
    methods: {
        readFileMultiple(file) {
            return URL.createObjectURL(file);
        }
    }
}

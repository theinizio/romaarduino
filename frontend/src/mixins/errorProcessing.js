export default {
    methods: {
        errorProcessing(status) {
            switch (status) {
                case 401: // Unauthorized
                    this.callToast('danger', `Ошибка ${status}`, 'Пользователь не авторизирован!', 5000);
                    break;
                case 404: // Not Found
                    this.callToast('danger', `Ошибка ${status}`, 'Страница не найдена!', 5000);
                    break;
                case 422: // Data is invalid
                    this.callToast('danger', `Ошибка ${status}`, 'Данные введены не верно!', 5000);
                    break;
                case 500: // Internal Server Error
                    this.callToast('danger', `Ошибка ${status}`, 'Ошибка сервера! Попробуйте позже.', 5000);
                    break;
                case 503: // Service Unavailable
                    this.callToast('danger', `Ошибка ${status}`, 'Сервис недоступен или перегружен! Попробуйте позже.', 5000);
                    break;
                default:
                    break;
            }
        }
    }
}

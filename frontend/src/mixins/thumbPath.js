export default {
    computed: {
        thumbPath() {
            const l = location;
            return `${l.protocol}//${l.hostname}/storage/`;
        }
    }
}
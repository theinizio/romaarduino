export default {
    data: () => ({
        preview_src: '',
        isValid: true
    }),
    methods: {
        parseVideoUrl(path) {
            if (path.match(/https:\/\/www.youtube.com\/w/)) {
                return `https://www.youtube.com/embed/${path.slice(32, 43)}`
            } else if (path.match(/https:\/\/www.youtube.com\/e/)) {
                return `https://www.youtube.com/embed/${path.slice(30, 41)}`
            } else if (path.match(/https:\/\/youtu.be/)) {
                return `https://www.youtube.com/embed/${path.slice(17, 28)}`
            } else if (path.match(/https:\/\/vimeo.com\//)) {
                return `https://player.vimeo.com/video/${path.slice(18, 27)}`
            } else if (path.match(/https:\/\/player.vimeo.com\/video/)) {
                return `https://player.vimeo.com/video/${path.slice(31, 40)}`
            } else {
                return false;
            }
        },

        setEmbedVideo(e) {
            const path = e.target.value
            if (path) {
                const preview_src = this.parseVideoUrl(path)
                this.preview_src = preview_src
                this.isValid = !!preview_src
            } else {
                this.preview_src = ''
                this.isValid = false
            }
        }
    }
}

/*
* https://vimeo.com/388640017
* https://youtu.be/jnMWIj8w1fI
* https://player.vimeo.com/video/388640017?title=0&byline=0&portrait=0
* https://www.youtube.com/watch?v=jnMWIj8w1fI
* * */
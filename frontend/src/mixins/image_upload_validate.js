export default {
    methods: {
        // stay this method
        isValidImageSize(file) {
            let size_bytes = file.size,
                size_megabytes = (size_bytes/1000000).toFixed(2);

            if (size_megabytes >= 10) {
                this.callToast(
                    'danger',
                    'Внимание!',
                    `Размер файла превышен! Максимальный размер - 10мб. Вы загрузили изображение размером - ${size_megabytes}мб`,
                    8000,
                );
                return false;
            }
            return true;
        },
        isValidImagesSize(size) {
            let size_bytes = size,
                size_megabytes = (size_bytes/1000/1000).toFixed(2);

            if (size_megabytes >= 100) {
                this.callToast(
                    'danger',
                    'Внимание!',
                    `Размер файлов превышен! Максимальный размер - 100мб. Вы загрузили изображения размером - ${size_megabytes}мб`,
                    8000,
                );
                return false;
            }
            return true;
        }
    }

}



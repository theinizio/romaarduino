export default {
    methods: {
        callToast(variant = null, title = 'Уведомление', toastBodyContent = null, hideDelay = 1000) {
            this.$bvToast.toast(toastBodyContent, {
                'title': title,
                'variant': variant,
                'solid': true,
                'autoHideDelay': hideDelay,
                'noCloseButton': false,
            })
        }
    }
}

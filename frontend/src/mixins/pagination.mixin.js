import _ from 'lodash';

export default {
    data() {
        return {
            page: +this.$route.query.page || 1,
            pageSize: 15,
            pageCount: 0,
            allItems: [],
            items: [],
            itemIndex: 0,

            // pagination
            current_page: +this.$route.query.page || 1,
            first_page_url: null,
            from: null,
            last_page: null,
            last_page_url: null,
            next_page_url: null,
            path: null,
            per_page: null,
            prev_page_url: null,
            to: null,
            total: null,

            // sort
            listSortTable: [15, 20, 25, 30]
        }
    },
    methods: {
        setupPagination(allItems) {
            this.allItems = _.chunk(allItems, this.pageSize);
            this.pageCount = _.size(this.allItems);
            this.items = this.allItems[this.page - 1] || this.allItems[0];
        },
        pageChangeHandler(page) {
            this.$router.push(`${this.$route.path}?page=${page}`);
            this.items = this.allItems[page - 1] || this.allItems[0];
        },
        watchRowIndex(count) {
            this.itemIndex = count > 1 ? this.pageSize * (count - 1) : 0;
        },

        getPaginationData(data) {
            console.log(data);

                    this.current_page =  data.page;
                    // first_page_url: this.first_page_url,
                    this.last_page =  data.lastPage;
                    // last_page_url: this.last_page_url,
                    // next_page_url: this.next_page_url,
                    // path: this.path,
                    this.per_page = data.perPage;
                    // prev_page_url: this.prev_page_url,
                    // from: this.from,
                    // to: this.to,
                    this.total =  data.total;

        },
    },
    mounted() {
        this.watchRowIndex(this.page);
    },
    watch: {
        page(count) {
            this.watchRowIndex(count);
        }
    }
}

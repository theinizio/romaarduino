export default function dateFilter(string) {
    const date = string.substr(0,10).split('-').reverse().join('.')
    const time = string.substr(11, 8)
    return `${date} ${time}`;
}

export default function getFormData(id) {
    return new FormData(document.getElementById(id))
}
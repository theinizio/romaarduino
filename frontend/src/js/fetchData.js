import axios from 'axios';
import store from '../store'
export default (method, path, data = null) => {
    const config = { headers: { Authorization: 'Bearer ' + store.getters.getToken} };
    const url = `${location.protocol}//${location.hostname}${path}`;
    switch(method) {
        case 'post':
            return axios.post(url, data, config).catch(err => err);
        case 'get':
            return axios.get(url, config).catch(err => err);
        case 'delete':
            return axios.delete(url, config).catch(err => err);
        case 'put':
            return axios.put(url, data, config).catch(err => err);
        default:
            break;
    }
}
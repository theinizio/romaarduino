export default function (current_instance, request_name, data = null) {
    return current_instance.$store.dispatch(request_name, data !== null ? data : '');
}
#include <Wire.h>
//#include <SPI.h>
//#include <Adafruit_GFX.h>
//#include <Adafruit_SSD1306.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Bounce2.h>


#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
//Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);


#define ONE_WIRE_BUS             2                                      // подключения DS18B20 на пине D4

#define drain_duration        7000                               // время слива воды = 2 секунды

#define long_press_delay      3500

#define num_modes                2                                              // максимальный номер режима

#define DELTA                    4.0
#define DELTA_collector          4.0
#define target_heater_temp      30.0                               //рабочая температура нагревателя
#define target_collector_temp   45.0                               //рабочая температура коллектора
#define collector_motor_work_time   15000


#define drain_relay              4                                     //клапан слива воды D2
#define collector_relay          5                                     //реле коллектора   D1
#define heater_relay             0                                     //реле нагревателя  D3
#define collector_led            13                                     //диод коллектора  D7
#define heater_led               15                                     //диод нагревателя D8

#define button                  16                                    //кнопка             D0

Bounce bouncer = Bounce();                                         //создаем экземпляр класса Bounce

String mode = "wait";

unsigned long timing;                                          // Переменная для хранения точки отсчета(пауза)

unsigned long press_time = 0;


OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
//28 AA 64 25 4A 14 1 A0
DeviceAddress Thermometer1 = {
        0x28, 0xAA, 0x64, 0x25, 0x4A, 0x14, 0x01, 0xA0        // адрес датчика 1 DS18B20 28 AA 61 81 4E 14 01 4B
};
//28 FF 7B 94 70 17 04 B6
DeviceAddress Thermometer2 = {
        0x28, 0xFF, 0x7B, 0x94, 0x70, 0x17, 0x04, 0xB6        // адрес датчика 2 DS18B20 28 AA 35 89 4E 14 01 DD
};


void setup() {
    Serial.begin(115200);

    //   SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
//    if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3c)) { // Address 0x3D for 128x64
        //Serial.println(F("SSD1306 allocation failed"));
        //        for (;;); // Don't proceed, loop forever
//    }

    // Show initial display buffer contents on the screen --
    // the library initializes this with an Adafruit splash screen.
    //  display.display();
    //  delay(200); // Pause for 2 seconds


    Wire.begin();
    sensors.begin();
    sensors.setResolution(Thermometer1, 8);
    sensors.setResolution(Thermometer2, 8);

    pinMode(button, INPUT);              // кнопка на пине 10
    digitalWrite(button, HIGH);          // подключаем встроенный подтягивающий резистор на пине 10
    bouncer.attach(button);             // устанавливаем кнопку на пине 10
    bouncer.interval(50);            // устанавливаем параметр stable interval = 3 мс



    pinMode(heater_led, OUTPUT);               // пин вкл/выкл. светодиода в режиме *Водонагреватель*
    pinMode(collector_led, OUTPUT);               // пин вкл/выкл. светодиода в режиме *Коллектор*
    pinMode(collector_relay, OUTPUT);              // пин вкл/выкл. реле в режиме *Коллектор*
    pinMode(heater_relay, OUTPUT);              // пин вкл/выкл. реле в режиме *Водонагреватель*
    pinMode(drain_relay, OUTPUT);              // пин вкл/выкл. реле в режиме *Слив воды*
//    test();
//    reDrawScreen();
    //    sensors.requestTemperatures();
    //    reDrawScreen("Collector", sensors.getTempC(Thermometer1));      // Draw characters of the default font

    Serial.print("Режим: ожидание;");
}

void loop() {

    sensors.requestTemperatures();
    checkMode();

    if (millis() % 2000 < 100) {                //Этот кусок кода выполняется раз в три секунды, примерно
        if (mode == "Heater")
            waterHeaterModeJob();

        if (mode == "Drain")
            drainModeJob();

        if (mode == "Collector")
            collectorModeJob();

//        reDrawScreen();
    }


}


void reDrawScreen() {
//    display.clearDisplay();
//    display.setTextSize(2);
//    display.setTextColor(SSD1306_WHITE);
//    display.cp437(true);
//
//    display.setCursor(2, 2);
//    String textLine = "mode:";
//    for (char i = 0; i < textLine.length(); i++) {
//        display.print(textLine[i]);
//    }
//    display.setCursor(2, 22);
//    textLine = mode;
//    for (char i = 0; i < textLine.length(); i++) {
//        display.print(textLine[i]);
//    }
//    display.setCursor(2, 47);
//    textLine = String(sensors.getTempC(Thermometer2));
//    for (char i = 0; i < textLine.length()-1; i++) {
//        display.print(textLine[i]);
//    }
//    display.write(0x20);
//    //    display.write(0x20);
//    //  display.write(0xF8);  //degree sign for eng font
//    //  display.write(9);         //some sign for russian font
//    display.drawCircle(58, 50, 3, SSD1306_WHITE);
//    display.write(0x43);
//
//    display.setTextSize(1);
//    display.setCursor(84, 52);
//    textLine = String(sensors.getTempC(Thermometer1));
//    for (char i = 0; i < textLine.length()-1; i++) {
//        display.print(textLine[i]);
//    }
//    display.write(0x20);
//    //    display.write(0x20);
//    //  display.write(0xF8);  //degree sign for eng font
//    //  display.write(9);         //some sign for russian font
//    display.drawCircle(111, 53, 1, SSD1306_WHITE);
//    display.write(0x43);
//
//
//
//    display.display();
}


void checkMode() {
    if (mode != "Drain") {   //нельзя переключить режим, пока сливает
        bouncer.update(); // Update the Bounce instance


        if (bouncer.fell() && press_time == 0) {   //нажали кнопку вниз
            press_time = millis();   //засекаем время
        }

        if (bouncer.rose() && press_time && millis() - press_time < long_press_delay) {
            press_time = 0;
            if (mode == "Collector")mode = "Heater"; else mode = "Collector";

            if (mode == "Heater") {
                Serial.println("Mode: Нагреватель");
//                reDrawScreen();
                digitalWrite(heater_led, HIGH);
                digitalWrite(collector_led, LOW);
                digitalWrite(collector_relay, LOW);
                digitalWrite(drain_relay, LOW);

            }
            if (mode == "Collector") {
                Serial.println("Mode: Коллекторррр");
//                reDrawScreen();
                digitalWrite(heater_led, LOW);
                digitalWrite(collector_led, HIGH);
                digitalWrite(heater_relay, LOW);
                digitalWrite(drain_relay, LOW);
            }
        }

        if (press_time && millis() - press_time >= long_press_delay) {
            mode = "Drain";
            press_time = 0;
            Serial.println("Mode: Слив водыыыыыыыыыыы");
//            reDrawScreen();
            digitalWrite(heater_led, HIGH);
            digitalWrite(collector_led, HIGH);
            digitalWrite(collector_relay, LOW);
            digitalWrite(heater_relay, LOW);
        }

    }
}


void collectorModeJob() {
    
    float collector_temp = sensors.getTempC(Thermometer1);
    float heater_temp = sensors.getTempC(Thermometer2);

    Serial.println("Режим: Коллектор;\n Sensor1  " + String(heater_temp));


  if (
        collector_temp <= heater_temp ||
        heater_temp >= target_heater_temp ||
        millis() - timing >= collector_motor_work_time) {
        Serial.println(" Выкл насос.");
        digitalWrite(collector_relay, LOW);
        timing = 0;
        delay(1000);
    }


    if(heater_temp > collector_temp || collector_temp < target_collector_temp ) return;


    if (
        collector_temp >= heater_temp &&
//        heater_temp <= target_collector_temp - DELTA &&
        (!timing || millis() - timing < collector_motor_work_time)
        ) {

        if (timing == 0) {
            timing = millis();
        }
        //Serial.println(" Вкл насос.");
        digitalWrite(collector_relay, HIGH);
        return;
    }

  
}


void waterHeaterModeJob() {
    float temp = sensors.getTempC(Thermometer2);
    Serial.println("Режим: Водонагреватель;\n   Sensor2  " + String(temp));

    if (temp >= target_heater_temp) {
        Serial.println(" Выкл нагрев воды.");
        digitalWrite(heater_relay, LOW);
    }

    if (temp <= target_heater_temp - DELTA) {
        Serial.println(" Вкл нагрев воды.");
        digitalWrite(heater_relay, HIGH);
    }
}

void drainModeJob() {
    digitalWrite(drain_relay, HIGH); //включаем реле
    Serial.print("Режим: Слив воды;");

    //TODO add progressbar
    delay(drain_duration);   //ждем
    digitalWrite(drain_relay, LOW); //закрываем клапан
    mode = "Wait";  //переходим в режим ожидания
    Serial.print("Режим: ожидание;");
//    reDrawScreen();
    digitalWrite(heater_led, LOW);
    digitalWrite(collector_led, LOW);
}


void test() {
    digitalWrite(heater_relay, HIGH);
    delay(250);
    digitalWrite(collector_relay, HIGH);
    delay(220);
    digitalWrite(heater_relay, LOW);
    delay(250);
    digitalWrite(drain_relay, HIGH);
    delay(220);
    digitalWrite(collector_relay, LOW);
    delay(250);
    digitalWrite(heater_led, HIGH);
    delay(220);
    digitalWrite(drain_relay, LOW);
    delay(250);
    digitalWrite(collector_led, HIGH);
    delay(220);
    digitalWrite(heater_led, LOW);
    delay(250);
    digitalWrite(collector_led, LOW);
}

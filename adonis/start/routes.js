'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')


Route.resource('/api/admin/user', 'UserController').middleware('admin')


Route
  .get('/users/:id', 'UserController.show')
  .middleware('admin')


Route.post('/api/login', 'AuthController.login').middleware('guest')

Route.post('/api/logout', 'AuthController.logout').middleware('auth')

Route.post('/api/register', 'RegisterController.doRegister')

Route.get('/api/login/facebook', 'LoginController.redirect')
Route.get('/api/facebook/callback', 'LoginController.callback')

Route.resource('/api/device' ,'DeviceController')
    .middleware('auth')
    .apiOnly()
    .formats(['json'])

Route.get('/api/get_data/:device_id', 'DatumController.show')
    .middleware('auth')

Route.get('/api/device_chart_data/:device_id', 'DatumController.chartData')
    .middleware('auth')

Route.resource('/api/timetable' ,'TimetableController')
    .middleware('auth')
    .apiOnly()
    .formats(['json'])

Route.resource('/api/errors' ,'ErrorController')
    .middleware('auth')
    .apiOnly()
    .formats(['json'])

Route.post('/api/send_mode_to_device', 'DeviceController.sendModeToDevice')
    .middleware('auth')
Route.post('/api/send_timetable_to_device', 'DeviceController.sendTimetableTodDevice')
    .middleware('auth')

Route.post('/api/set_device_owner', 'UserController.addDevice')

Route.post('/api/static_page/save_all', 'StaticPageController.savePages')
Route.get('/api/static_page', 'StaticPageController.index')
Route.get('/api/static_page/:page:slug', 'StaticPageController.show')

Route.put('/api/static_page/:page_id', 'StaticPageController.update')

Route.post('/api/static_page/:page_id', 'StaticPageController.create')
Route.delete('/api/static_page/:page_id', 'StaticPageController.delete')




'use strict'

class AddDevice {
  get rules () {
    return {
      device_id: 'required|number',
      user_id: 'required|number',
    }
  }
}

module.exports = AddDevice

'use strict'
const User = use('App/Models/User');
const Device = use('App/Models/Device')
const { validate } = use('Validator')
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with users
 */
class UserController {
  /**
   * Show a list of all users.
   * GET users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({request, response, view}) {
    const User = use('App/Models/User')
    const page = request.input('page')
    return  await User.query().orderBy('created_at' ,'desc').paginate(page)
  }

  /**
   * Render a form to be used for creating a new user.
   * GET users/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({request, response, view}) {
    const password = Math.random().toString(36).slice(-12)
    return view.render('users.create', {password: password});
  }

  /**
   * Create/save a new user.
   * POST users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({request, response, view}) {
    const rules = {
      name: 'required|string|min:3',
      email: 'required|email|unique:users,email',
      password: 'required|string|min:8',
      role: 'required|string|in:admin,user',
    }
    const validation = await validate(request.all(), rules)
    if (validation.fails()) {
      console.log(validation.messages())
      return view.render('users.create', {errors:validation.messages()})
    }
    const user = new User();
    user.name = request.input('name');
    user.email = request.input('email');
    user.password = request.input('password');
    user.role = request.input('role');
    console.log(await user.save())
    response.route('users.index')
  }

  /**
   * Display a single user.
   * GET users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({params, request, response, view}) {
  }

  /**
   * Render a form to update an existing user.
   * GET users/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({params, request, response, view}) {
    const user = await User.findBy('id', params.id);
    return view.render('users.edit', {user: user});
  }

  /**
   * Update user details.
   * PUT or PATCH users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({params, request, response, view}) {
    const rules = {
      name: 'required|string|min:3',
      email: 'required|email|unique:users,email',
      role: 'required|string|in:admin,user',
    }
    const validation = await validate(request.all(), rules)
    if (validation.fails()) {
      console.log(validation.messages())
      return  {errors:validation.messages()};
    }
    const user = await User.findBy('id', params.id);
    user.name = request.input('name');
    user.email = request.input('email');
    user.role = request.input('role');
    console.log(params, await user.save(), user)
    response.route('users.index')
  }

  /**
   * Delete a user with id.
   * DELETE users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({params, request, response}) {
    const user = await User.findBy('id', params.id);
    console.log(await user.delete())
    response.route('users.index')
  }

  /**
   * Adds device to user
   * POST /users/add_device/
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @returns {Promise<void>}
   */
  async addDevice({params, request, auth, response}) {

    const rules = {
      name: 'required|string|min:3',
      password: 'required|string|min:3',
    }
    const validation = await validate(request.all(), rules)
    if (validation.fails()) {
      console.log(validation.messages())
      return  {errors:validation.messages()};
    }
    const device = await Device.query().where(request.post()).first()
    const user =  await auth.getUser();
    console.log(device, user);
    if(device && user) {
      return response.json(await user.devices().save(device))
      // return response.json(await device.user().attach(user.id))
    }
    return response.status(404).json('wrong device name and password');
  }

}

module.exports = UserController

'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Timetable = use('App/Models/Timetable')
/**
 * Resourceful controller for interacting with timetables
 */
class TimetableController {
  /**
   * Show a list of all tmetables.
   * GET tmetables/:device_id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ params, request, response, view }) {
    console.log(params.device_id);
    return await Timetable
        .query()
        .where('device_id', '=', params.device_id)
        .orWhere('id', '=', '1')
        .fetch()
  }

  /**
   * Create/save a new tmetable.
   * POST tmetables
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    return response.json(await Timetable.create(request.all()))
  }

  /**
   * Display a single tmetable.
   * GET tmetables/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    // return response.json(await Timetable.find(params.id) || 'not found')
    console.log(params)
    return await Timetable
        .query()
        .where('device_id', '=', params.id)
        .orWhere('id', '=', '1')
        .fetch()
  }

  /**
   * Update tmetable details.
   * PUT or PATCH tmetables/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
      const timetable = await Timetable.find(params.id)
    if(timetable){
      return response.json(await timetable.update(request.all()))
    }
  }

  /**
   * Delete a tmetable with id.
   * DELETE tmetables/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    if(params.id === 1){
      return false
    }
    const timetable = await Timetable.find(params.id)
    if (timetable && timetable.device_id != 0) {
      return response.json(await timetable.delete())
    }
    return response.json('not found')
  }
}

module.exports = TimetableController

'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const StaticPage = use('App/Models/StaticPage')
/**
 * Resourceful controller for interacting with staticpages
 */
class StaticPageController {
  /**
   * Show a list of all static_page.
   * GET static_page
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    return response.json(await StaticPage.all())
  }

  /**
   * Render a form to be used for creating a new staticpage.
   * GET staticpages/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {

  }

  /**
   * Create/save a new staticpage.
   * POST static_page
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single staticpage.
   * GET static_page/:page_slug
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    return  await StaticPage.findBy('slug', params.page_slug).first();
  }

  /**
   * Render a form to update an existing staticpage.
   * GET staticpages/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update staticpage details.
   * PUT or PATCH staticpages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a staticpage with id.
   * DELETE staticpages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async savePages ({ params, request, response }) {
    console.log(123);
    const newPages = request.all().pages
    console.log(newPages)
    const oldPages = await StaticPage.all()
    console.log(oldPages)
    Array.from(oldPages.rows || []).map(page=>page.delete());

    return response.json(newPages.map(page => StaticPage.create(page)))
  }
}

module.exports = StaticPageController

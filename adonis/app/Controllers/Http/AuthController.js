'use strict'
const User = use('App/Models/User')
const Hash = use('Hash')

class AuthController {
  async register({request, auth, response}) {

    let user = await User.create(request.all())

    //generate token for user;
    let token = await auth .withRefreshToken().generate(user)

    Object.assign(user, token)

    return response.json(user)
  }

  async login({request, auth, response}) {

    let {email, password} = request.all();
    try {
      if (await auth.attempt(email, password)) {
        let user = await User.findBy('email', email)
        let token = await auth .withRefreshToken().generate(user)

        return response.json({...user, ...token})
      }
    }
    catch (e) {
      console.log(e)
      return response.status(401).json({message: 'You are not registered!'})
    }
  }

  async logout({request, auth, response}) {
    return await auth.logout();
  }

  async passwordReset({request, auth, response}) {

  }
  async changePassword({request, auth, response}){
    const user  = await auth.user();
    const oldPassword = request.input('old_password');
    const passwordsMatch = await Hash.verify(oldPassword, user.password)
    console.log(user, oldPassword, passwordsMatch);
  }

  async refreshToken({request, auth, response}){
    const refreshToken = request.input('refresh_token')
    return await auth.generateForRefreshToken(refreshToken, true)
  }

}

module.exports = AuthController

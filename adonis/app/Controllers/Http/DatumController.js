'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */


/** @typedef {import('@adonisjs/framework/src/View')} View */
const Data = use('App/Models/Datum')
const Device = use('App/Models/Device')
/**
 * Resourceful controller for interacting with data
 */
class DatumController {

  /**
   * Create/save a new datum.
   * POST data
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {

    return  response.json(await Data.create(request.all()));
  }

  /**
   * Display a single datum.
   * GET data/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {

      if(!params.device_id) {
        return response.status(404);
      }

      const data =await Data
        .query()
        .where('device_id', params.device_id)
        .with('device')
        .last()

      const result = data.toJSON();
      result.isOnline = (await Device.deviceStatus(params.device_id)).data;

      return result;
  }

  /**
   * Delete a datum with id.
   * DELETE data/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const data = await Data.find(params.id)
    if(data){
      return response.json(data.destroy())
    }
    response.json('destroy failed')
  }

  async chartData ({ params, request, response }) {

    return await Data.findBy('device_id', params.device_id).select('heater_temp').limit(200).orderBy('created_at', 'desc').fetch()
  }

}

module.exports = DatumController

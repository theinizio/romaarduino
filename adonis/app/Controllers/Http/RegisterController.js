
'use strict'

const User = use('App/Models/User')
const Hash = use('Hash')

class RegisterController {
  index({request, response, view}) {
    return view.render('register')
  }

  async doRegister({request, auth, response}) {
    try {
      const user = new User()
      user.name = request.input('name')
      user.email = request.input('email')
      user.password = request.input('password')
      await user.save()

      if (await auth.attempt(request.input('email'), request.input('password'))) {

        let token = await auth.withRefreshToken().generate(user)
        Object.assign(user, token)
        return response.json(token)
      }
    }
      catch (e) {
        console.log(e)
        return response.json({...e, message: 'You are not registered!'})
      }
    return  'Registration Successful! Now go ahead and login'

  }
}
module.exports = RegisterController

'use strict'

const Database = use('Database')
const Device = use('App/Models/Device')
const Timetable = use('App/Models/Timetable')
const axios = use('axios'); // Adonis request method
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Controller for interacting with devices
 */
class DeviceController {
  /**
   * Show a list of all devices.
   * GET devices
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, auth,  response, view }) {
    try {
      const user =  await auth.getUser()
      const page = request.input('page')
      const devices = user.role === 'admin' ? await Device.query().with('user').paginate(page) : await user.devices().paginate(page)

      if(devices.pages.total) {
        const deviceArray = devices.toJSON();
        deviceArray.data = await Device.devicesStatuses(deviceArray.data);
        return  deviceArray;
      }

      return response.json(devices);

    } catch (error) {
      console.log(error)
      return response.send(error)
    }
  }

  /**
   * Create/save a new device.
   * POST devices
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    return response.json(await Device.create(request.all()))
  }

  /**
   * Update device details.
   * PUT or PATCH devices/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const device = await Device.find(params.id)
    return response.json(await device.update(request.all()))
  }

  /**
   * Delete a device with id.
   * DELETE devices/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const device = await Device.find(params.id)
    if(device) {
      return response.json(await device.delete())
    }
    return response.json('not found')
  }

  async sendTimetableToDevice({ params, request, auth, response }) {
    const device = await Device.find(params.id)
    const user = await auth.getUser()
    const tt = await Timetable.findBy('device_id', params.id);

    if(device && await user.isDeviceOwned(device)) {
      const {data} = await Request.post('http://socket/device/send_tt', request.all())
      return data
    }
    return 'device not found'
  }

  async sendModeToDevice({ params, request, auth, response }) {
    const device = await Device.find(request.input('device_id'))
    const user = await auth.getUser()

    if(device && await user.isDeviceOwned(device)) {
        await this.sleep(2000);
        const deviceState = await device.datum().last();
        let result = {};
        console.log(deviceState);
        if(deviceState){
          result = deviceState.toJSON();
          result.isOnline = (await Device.deviceStatus(device.id)).data;
        }

        const {data} = await axios.post('http://socket:3000/device/send_mode', request.all())

        return {status: data, ...result, device: device.toJSON()};
    }
    return 'device not found'
  }

  sleep(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }

}

module.exports = DeviceController

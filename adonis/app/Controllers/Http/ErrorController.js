'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Error = use('App/Models/Error')
/**
 * Resourceful controller for interacting with errors
 */
class ErrorController {
  /**
   * Show a list of all errors.
   * GET errors
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index ({ request, response}) {

  }

  /**
   * Create/save a new error.
   * POST errors
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const rules = {
      device_id: 'required|number|min:1',
      description: 'required|string',
    }
    const validation = await validate(request.all(), rules)
    if (validation.fails()) {
      return validation.messages()
    }
    return await Error.create(request.all())
  }

  /**
   * Display a single error.
   * GET errors/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show ({ params, request, response }) {
    return await Error.query().where('device_id', params.id).pickInverse()
  }

  /**
   * Delete a error with id.
   * DELETE errors/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const error = await Error.find(params.id)
    if(error) {
      return response.json(await error.delete())
    }
    return response.json('not found')
  }
}

module.exports = ErrorController

'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

const Mode = use('App/Models/Mode')

class ModeController {
  /**
   * Show a list of all modes.
   * GET modes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index ({ request, response }) {

    return response.json(await Mode.all())
  }

}

module.exports = ModeController

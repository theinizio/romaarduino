'use strict'

class SoketController {
    constructor({socket, request}) {
      console.log('in SocketController', socket)
        this.socket = socket
        this.request = request
    }

    onNewUser(user){
      console.log('new user', user)
    }

    onUserLeft(user) {
      console.log('user left', user)
    }

  joinRoom (room) {
      console.log('joining', room)
    const user = this.socket.currentUser
  }
  leaveRoom (room) {
   console.log('leave room')
  }

    onMessage(message) {
      console.log(message)
      try {
        let result = message.toString()
        result = result.replace('\u0000', '').split('').join('')
        const json = JSON.parse(result)
        handleMessage(socket, json)
      } catch (e) {
        console.log(e)
      }
    }

    onClose() {
      console.log('closing')
      this.connectedUsers = connectedUsers.filter((user) => {
        if (user.socket === socket) {
          console.log(`${user.name} disconnected`)
          return false
        }
        return true
      })
    }

    onError(error) {
      console.log('socket error', error)
    }


  /**
   * Processes the incoming message.
   * @param socket The socket that sent the message
   * @param message The message itself
   */
  async  handleMessage(socket, message) {

    switch (message.channel) {
      case "login":
        try {
          const [[result]] = await findOrCreateDevice(message.name, message.password)
          const user = {socket, name: message.name, ...result}
          console.log(`${message.name} joined`, user)
          connectedUsers.push(user)
          send({type: 1, id: result.id}, user.id)
        } catch (e) {
          console.log(e)
        }
        break

      case "data":
        console.log(` data from  ${message.id} `)
        saveData(message)
        break

      case "error":
        console.log(`error from  ${message.id} `)
        handleError(message)
        break

      default:
        console.log("unknown message", message)
        break
    }
  }

  async  saveData(message) {
    try {
      const {id, device_time, collector_temp, heater_temp, inside_temp, collector_relay, heater_relay, drain_relay, mode} = message

      return await pool.query(
          `insert into data (device_id, device_time, collector_temp, heater_temp, inside_temp,
                                       collector_relay,
                                       heater_relay, drain_relay, mode)
                     values (?, ?, ?, ?, ?, ?, ?, ?, ?)`,
          [id, device_time, collector_temp, heater_temp, inside_temp, collector_relay, heater_relay, drain_relay, mode]
      )
    }catch (e) {
      console.log(e)
    }
  }

  async  handleError(message) {
    try {
      const {id, error} = message
      return await pool.query(
          'insert into errors (device_id, error) values (?, ?)',
          [id, error],
      )
    }catch (e) {
      console.log(e)
    }
  }

  async  findOrCreateDevice(name, password) {
    return await pool.query(
        'select * from devices where name=? and password = ?', [name, password])
  }

}

module.exports = SoketController
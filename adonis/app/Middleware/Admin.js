'use strict'

class Admin {
  async handle ({ request,  response, auth }, next) {
    if(auth.user.role == 'admin') {
      await next()
    }else{
      console.log('only for admins')
      response.route('home', {errors:{message: "only for admins!"}})
    }
  }
}

module.exports = Admin

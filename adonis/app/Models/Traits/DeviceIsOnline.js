const axios = require('axios')

class DeviceIsOnline {
  register (Model, customOptions = {}) {
    const defaultOptions = {}
    const options = Object.assign(defaultOptions, customOptions)
    console.log(options);
    Model.devicesStatuses = async (devices)=> {
      return Promise.all(
        devices.map(async device => {
          const result = await axios.get('http://socket:3000/device/is_online?device_id=' + device.id)
          device.isOnline = result.data;
          return device;
        }))
    }

    Model.deviceStatus = async (deviceId) => {
      return await axios.get('http://socket:3000/device/is_online?device_id=' + deviceId)
    }

    Model.prototype.devicesStatuses = async (devices)=> {
      return Promise.all(
        devices.map(async device => {
          const result = await axios.get('http://socket:3000/device/is_online?device_id=' + device.id)
          device.isOnline = result.data;
          return device;
        }))
    }

    Model.prototype.deviceStatus = async (deviceId) => {
      return await axios.get('http://socket:3000/device/is_online?device_id=' + deviceId)
    }
  }

}

module.exports = DeviceIsOnline

'use strict'
/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')
/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const axios = use('axios')

class Device extends Model {

  static boot () {
    super.boot()
    this.addTrait('DeviceIsOnline')
  }

  static get hidden() {
    return ['updated_at']
  }

  datum() {
    return this.hasMany('App/Models/Datum');
  }

  errors() {
    return this.hasMany('App/Models/Error');
  }

  timetables() {
    return this.hasMany('App/MOdels/Timetable');
  }

  mode() {
    return this.hasOne('App/Models/Mode');
  }

  user() {
    return this.belongsToMany('App/Models/User').pivotTable('users_devices').withTimestamps();
  }

  async getIsOnline() {
        return  await axios.get('http://socket:3000/device/is_online?device_id=' + this.id)
  }

}

module.exports = Device

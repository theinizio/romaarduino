'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Datum extends Model {
    device() {
        return this.belongsTo('App/Models/Device');
    }

}

module.exports = Datum

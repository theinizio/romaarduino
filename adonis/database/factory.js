const Factory = use('Factory')
const Hash = use('Hash')
const Slug = use('slug');

Factory.blueprint('App/Models/User', async (faker, i, data) => {
  return {
    name: data.name || faker.username(),
    email: data.email || faker.email(),
    password: data.password ||'password',
    role: data.role || 'user',
  }
})

Factory.blueprint('App/Models/Timetable', async  (faker, i, data) => {
  return {
    device_id: data.device_id || 0,
    mode_id: data.mode_id || 0,
    days_of_week: data.days_of_week ||127,
    from_time: data.from_time || "00:00:00",
    till_time: data.till_time || "23:57:00",
    temp: data.temp || 25.0,
  }
})

Factory.blueprint('App/Models/Mode', async  (faker, i, data) => {
  return {
    name: data.name || 'test'
  }
})

Factory.blueprint('App/Models/Device', async  (faker, i, data) => {
  return {
    name: data.name || 'esp82669995ROM1',
    password: data.password || "123456",
    description: data.description || faker.animal(),
  }
})

Factory.blueprint('App/Models/StaticPage', async  (faker, i, data) => {
  return {
    name: data.name || 'Контакты',
    slug: Slug(data.name || 'Контакты'),
    content: data.content || "<p>Телефон: (050)123-45-67</p><p>Адрес: Москава, Шаболовка, д.13</p>",
  }
})

'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DataSchema extends Schema {
  up () {
    this.create('data', (table) => {
      table.increments()
      table.integer('device_id')
      table.timestamp('device_time')
      table.decimal('collector_temp', 7, 1)
      table.decimal('heater_temp',    7, 1)
      table.decimal('inside_temp',    7, 1)
      table.boolean('collector_relay')
      table.boolean('heater_relay')
      table.boolean('drain_relay')
      table.string('mode', 25)
      table.timestamps()
    })
  }

  down () {
    this.drop('data')
  }
}

module.exports = DataSchema

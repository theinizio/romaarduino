'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ErrorSchema extends Schema {
  up () {
    this.create('errors', (table) => {
      table.increments()
      table.integer('device_id')
      table.string('description')
      table.timestamps()
      table.timestamp('deleted_at')
    })
  }

  down () {
    this.drop('errors')
  }
}

module.exports = ErrorSchema

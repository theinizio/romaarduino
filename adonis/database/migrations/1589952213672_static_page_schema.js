'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StaticPageSchema extends Schema {
  up () {
    this.create('static_pages', (table) => {
      table.increments()
      table.string('name')
      table.string('slug')
      table.text('content')
      table.timestamps()
    })
  }

  down () {
    this.drop('static_pages')
  }
}

module.exports = StaticPageSchema

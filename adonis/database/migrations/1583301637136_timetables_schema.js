'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TimetableSchema extends Schema {
  up () {
    this.create('timetables', (table) => {
      table.increments()
      table.integer('device_id')
      table.integer('mode_id').nullable()
      table.integer('days_of_week')
      table.time('from_time')
      table.time('till_time')
      table.decimal('temp')
      table.timestamps()
    })
  }

  down () {
    this.drop('timetables')
  }
}

module.exports = TimetableSchema

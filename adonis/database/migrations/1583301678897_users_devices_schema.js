'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersDevicesSchema extends Schema {
  up () {
    this.create('users_devices', (table) => {
      table.increments()
      table.integer('user_id')
      table.integer('device_id')
      table.timestamps()
    })
  }

  down () {
    this.drop('users_devices')
  }
}

module.exports = UsersDevicesSchema

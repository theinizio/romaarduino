'use strict'

/*
|--------------------------------------------------------------------------
| TimetableSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')

class TimetableSeeder {
  async run () {
    const timetables = await Database.table('timetables')
    const firstTimetable = await Factory.model('App/Models/Timetable').create()


  }
}

module.exports = TimetableSeeder

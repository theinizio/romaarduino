'use strict'

/*
|--------------------------------------------------------------------------
| ModeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')

class ModeSeeder {
  async run () {
    const timetables = await Database.table('modes')
    await Factory.model('App/Models/Mode').create({name: "wait"})
    await Factory.model('App/Models/Mode').create({name: "collector"})
    await Factory.model('App/Models/Mode').create({name: "heater"})
    await Factory.model('App/Models/Mode').create({name: "drain"})
  }
}

module.exports = ModeSeeder

const Factory = use('Factory')
const Database = use('Database')
const Hash = use('Hash')

class UserSeeder {
  async run() {
    const users = await Database.table('users')

    const usersArray = await Factory
      .model('App/Models/User')
      .createMany(5)

    const admin = await Factory
      .model('App/Models/User')
      .create({
      name: 'Admin Test',
      email: 'admin@test.com',
      password: '123456789',
      role: 'admin'
    })
  }
}

module.exports = UserSeeder

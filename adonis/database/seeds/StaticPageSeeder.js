'use strict'

/*
|--------------------------------------------------------------------------
| StaticPageSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')

class StaticPageSeeder {
  async run () {
    const staticPage = await Database.table('static_pages')
    await Factory.model('App/Models/StaticPage').create({name: "Контакты", content: "<p>Телефон: (050)123-45-67</p><p>Адрес: Москава, Шаболовка, д.13</p>"})
    await Factory.model('App/Models/StaticPage').create({name: "О нас", content: " <p>Привет. Мы производим солнечные колекторы.</p>"})
  }
}

module.exports = StaticPageSeeder

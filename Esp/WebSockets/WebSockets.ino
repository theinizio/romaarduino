#include "FS.h"
#include <Arduino.h>
#include <Hash.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <WebSocketsClient.h>
#include <Arduino_JSON.h>

#include <ezTime.h>
#include <NTPClient.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>


const String ID = "esp82669995ROM2";
const String pass = "123456";




//28 AA 64 25 4A 14 1 A0
DeviceAddress collector_temp_sensor_addr = {
        0x28, 0xAA, 0x64, 0x25, 0x4A, 0x14, 0x01, 0xA0        // адрес датчика 1 DS18B20 28 AA 61 81 4E 14 01 4B
};
//28 FF 7B 94 70 17 04 B6
DeviceAddress heater_temp_sensor_addr = {
        0x28, 0xFF, 0x7B, 0x94, 0x70, 0x17, 0x04, 0xB6        // адрес датчика 2 DS18B20 28 AA 35 89 4E 14 01 DD
};
//28 FF 7B 94 70 17 04 B0
DeviceAddress inside_temp_sensor_addr = {
        0x28, 0xFF, 0x7B, 0x94, 0x70, 0x17, 0x04, 0xB0        // адрес датчика 2 DS18B20 28 AA 35 89 4E 14 01 DD
};




const char* wlan_ssid             = "Gotovo";
const char* wlan_password         = "pogorelovo";
const char* ap_ssid               = "ESPap";
const char* ap_password           = "thereisnospoon";

const char* ws_host             = "185.185.70.38";
//const char* ws_host               = "192.168.0.101";
const int   ws_port               = 80;
const char* stompUrl            = "/ws";


String id = "0";
String mode = "wait";
String tt = "";
String old_data;

String last_payload = String("");
int httpTemp=0;
int wifi_reconnect_count = 0;
int status, vcc;
bool sockets_connected = false;


unsigned long motor_started;                                          // Переменная для хранения времени включения насоса
unsigned long motor_stopped;                                          // время выключения насоса



#define ONE_WIRE_BUS             2                                      // подключения DS18B20 на пине D4
#define drain_duration        7000                               // время слива воды = 2 секунды
#define long_press_delay      3500
#define num_modes                2                                              // максимальный номер режима
#define DELTA                    4.0
#define DELTA_collector          4.0
                      
#define target_collector_temp   45.0                               //рабочая температура коллектора
#define collector_motor_work_time   15000
#define max_temp                75.0
#define drain_relay              4                                     //клапан слива воды D2
#define collector_relay          5                                     //реле коллектора   D1
#define heater_relay             0                                     //реле нагревателя  D3
#define collector_led            13                                     //диод коллектора  D7
#define heater_led               15                                     //диод нагревателя D8
#define ISO8601        "Y-m-d\\TH:i:sO"
#define collector_motor_work_time    20000
#define collector_motor_work_delay   10000
#define collector_heater_temp_difference    8.0                       // разница температур для включения насоса коллектора

const String timetableFileName =      "/tt.txt";
const String ssidFileName =           "/ssid.txt";
const String passFileName =          "/pass.txt";
const String modeFileName =          "/mode.txt";

OneWire  ds(2);
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

Timezone myTZ;
WebSocketsClient webSocket;

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, 2);

ESP8266WebServer server(80);
bool ap_connected = true;

String script = String("<script src=\"https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js\"></script>") +
                String("<style>.pad,.w_10em{display:inline-block}#main,.bubble{text-align:end}body{font-family:'Trebuchet MS','Lucida Sans Unicode','Lucida Grande','Lucida Sans',Arial,sans-serif}body,button,input{font-size:x-large}.nice{color:#006400}.yellow_bg:hover{background-color:#ff0;cursor:pointer}.bubble{border:1px solid #000;border-radius:2px;background:wheat;padding:15px}.unvisible{z-index:5;position:absolute;opacity:0;transition:opacity .3s;transition:ease-in-out .4s}code{font-family:monospace}#header{margin-right:3px}pre{width:min-content;padding:20px!important}.w_10em{width:6em;margin-right:10px}.pad{margin:5px}#main{width:fit-content}</style>") +
                String("<script>var baseURL=window.location.protocol+'//'+window.location.host+'/';function connect(txt,id){var el=document.getElementById(id);if(el.innerText.substring(0,1)=='*')return;var bal=document.getElementById('bubble');") +
                String("var title=document.getElementById('header');var button=document.getElementById('conn_wifi');var pass=document.getElementById('wifi_pass');pass.value='';el=el.getBoundingClientRect();bal.style.top=el.bottom;bal.style.left=el.left+22;bal.style.opacity=1;tt=el;") +
                String("txt=txt.trim();title.innerText=txt;button.onclick=function(e){window.location=baseURL+'/?wifi_name='+txt+'&pass='+pass.value}}function save(){var ftp_n=document.getElementById('ftp_n').value;var ftp_p=document.getElementById('ftp_p').value;") +
                String("var host=document.getElementById('host').value;console.log(ftp_n,ftp_p,host);window.location=baseURL+'/?ftp_n='+ftp_n+'&ftp_p='+ftp_p+'&host='+host}if(window.location!=baseURL){console.log(window.location);console.log(baseURL);window.location=baseURL}") +
                String("var x;async function te(){Array.prototype.forEach.call(document.querySelectorAll(\".yellow_bg\"),function(e){x=e.children;console.log(x.length)})}document.addEventListener('DOMContentLoaded',function(){console.log(\"orig_ready!\");te()});$(function(){console.log(\"jquery_ready!\");te()})</script>") +
                String("<body>") +
                String("<div class=\"bubble unvisible\" id=bubble> <div> <span>Name: &nbsp;</span> <span id=header></span> </div><label for=\"wifi_pass\">Password </label><input type=password name=wifi_pass id=wifi_pass ><br><button id=conn_wifi onclick=\"connect_wifi();\" >Connect</button> </div>");

class relay {
  public:
    byte addr = 4;
    boolean state = 0;
    void turn() {
      this->state = !this->state;
      Serial.println("new relay state" + this->state);
    }
};

struct timetable {
  int id;
  int what;
  String days_of_week;
  float from_time;
  float till_time;
  int temp;
};

struct sensor {
  String addr;
  float value;
};


int target_heater_temp = 30; //рабочая температура нагревателя
ADC_MODE(ADC_VCC);

void setup() {


    Serial.begin(115200);
//        Serial.setDebugOutput(true);
    Serial.println();

    pinMode(heater_led, OUTPUT);               // пин вкл/выкл. светодиода в режиме *Водонагреватель*
    pinMode(collector_led, OUTPUT);               // пин вкл/выкл. светодиода в режиме *Коллектор*
    pinMode(collector_relay, OUTPUT);              // пин вкл/выкл. реле в режиме *Коллектор*
    pinMode(heater_relay, OUTPUT);              // пин вкл/выкл. реле в режиме *Водонагреватель*
    pinMode(drain_relay, OUTPUT);              // пин вкл/выкл. реле в режиме *Слив воды*
    
    SPIFFS.begin();
    

    sensors.begin();
    if (!sensors.getAddress(inside_temp_sensor_addr, 0)){
        Serial.println("Unable to find address for Device 0");
    } else{
        sensors.setResolution(inside_temp_sensor_addr, 9);
    }

    sensors.setResolution(collector_temp_sensor_addr, 9);
    sensors.setResolution(heater_temp_sensor_addr, 9);

    mode = getMode();

   
    Serial.print("Logging into WLAN: "); Serial.print(get_ssid()); Serial.print(" ..");

    WiFi.mode(WIFI_STA);

    WiFi.begin(get_ssid(), get_pass());

    
    while (WiFi.status() != WL_CONNECTED) {
        delay(200);
        Serial.print(".");
        if (wifi_reconnect_count++ > 100) {
          ap_connected = false;
          break;
        }
    }

    if(ap_connected){
        Serial.println(" success.");

        webSocket.begin(ws_host, ws_port, stompUrl);
        webSocket.onEvent(webSocketEvent);
        webSocket.setAuthorization("esp82669995ROM1", "123456");

        waitForSync();
        myTZ.setLocation(F("Europe/Kiev"));
        
    }
//    else{
    start_internal_server();
//    }
}

void loop() {
    sensors.requestTemperatures();


  
    if (ap_connected && WiFi.status() == WL_CONNECTED && millis() % 3000 < 100 ) {
        selectMode();
        webSocketLoop();
        
    }
//    else {
        server.handleClient();
//    }
}

void webSocketLoop() {
  webSocket.loop();
  if(sockets_connected){
      if(id != "0") {
          sendData();
      }else {
          Serial.println("received wrong device id: "+id+ ' ' + (id != "0") + (millis() % 3000 < 100 ));
          sendLoginMessage();
      }
  }
}

JSONVar gatherData() {
    JSONVar data;
    data["channel"] = "data";
    data["id"] = id;
    data["name"] = ID;
    data["collector_temp"] = getTemp(collector_temp_sensor_addr);
    data["heater_temp"] = getTemp(heater_temp_sensor_addr);
    data["inside_temp"] = getTemp(inside_temp_sensor_addr);
    data["collector_relay"] = 0;
    data["heater_relay"] = 0;
    data["drain_relay"] = 0;
    data["mode"] = mode;
    return data;
}


void sendData(){

    JSONVar data = gatherData();
    String msg = JSON.stringify(data);
    if(msg != old_data){
        old_data = msg;
        data["device_time"] = myTZ.dateTime("Y-m-d H:i:s");
        msg = JSON.stringify(data);
        Serial.println("Sending: " + msg);
        sendMessage(msg);
    }
}


void selectMode() {
  Serial.println("mode="+mode);
    if (mode.equals("heater"))
        waterHeaterModeJob();

    if (mode.equals("drain"))
        drainModeJob();

    if (mode.equals("collector"))
        collectorModeJob();
}


float getTemp(DeviceAddress deviceAddress){
  float tempC = sensors.getTempC(deviceAddress);
  if(tempC == DEVICE_DISCONNECTED_C){
    Serial.println("Error: Could not read temperature data");
    return -273.0;
  }
  return tempC;
}

String getAddr(DeviceAddress deviceAddress){
  String result="";
  for (uint8_t i = 0; i < 8; i++) {
    if (deviceAddress[i] < 16) result+=("0");
    result+=String(deviceAddress[i], HEX);
  }
  return result;
}



 

void collectorModeJob() {
    
    float collector_temp = getTemp(collector_temp_sensor_addr);
    float heater_temp = getTemp(heater_temp_sensor_addr);

    Serial.println("Режим: Коллектор;\n collector=" + String(collector_temp)+" heater="+String(heater_temp));

  if (
        heater_temp > max_temp ||
        collector_temp <= heater_temp ||
        heater_temp >= target_heater_temp + DELTA ||
        motor_started &&(millis() - motor_started >= collector_motor_work_time)
    ) {
        Serial.println(" Выкл насос.");
        digitalWrite(collector_relay, LOW);
        motor_started = 0;
        motor_stopped = millis();
//        delay(1000);
        return;
    }

    if(
      heater_temp >= collector_temp ||
      millis() - motor_stopped < collector_motor_work_delay
    ){
        return;
    }

    if (
        heater_temp <= max_temp - DELTA &&
        collector_temp - heater_temp >= collector_heater_temp_difference &&
//        heater_temp <= target_collector_temp - DELTA &&
        (!motor_started || millis() - motor_started < collector_motor_work_time)
     ) {

          if (motor_started == 0) {
              motor_started = millis();
          }
          //Serial.println(" Вкл насос.");
          digitalWrite(collector_relay, HIGH);
          return;
    }

  
}


void waterHeaterModeJob() {

    parseTimetable();

    float temp = getTemp(heater_temp_sensor_addr);

    //Serial.println("Режим: Водонагреватель;\n   Sensor2  " + String(temp));

    if (temp >= target_heater_temp) {
        //Serial.println(" Выкл нагрев воды.");
        digitalWrite(heater_relay, LOW);
    }

    if (temp <= target_heater_temp - DELTA) {
        //Serial.println(" Вкл нагрев воды.");
        digitalWrite(heater_relay, HIGH);
    }
}

void drainModeJob() {
    digitalWrite(drain_relay, HIGH); //включаем реле
    //Serial.print("Режим: Слив воды;");

    //TODO add progressbar
    delay(drain_duration);   //ждем
    digitalWrite(drain_relay, LOW); //закрываем клапан
    mode = "Wait";  //переходим в режим ожидания
    //Serial.print("Режим: ожидание;");
    
    digitalWrite(heater_led, LOW);
    digitalWrite(collector_led, LOW);
}


void sendMessage(String & msg) {
    webSocket.sendTXT(msg.c_str(), msg.length() + 1 );
}

void sendLoginMessage(){
  JSONVar myObject;

  myObject["channel"] = "login";
  myObject["name"] = ID;
  myObject["password"] = pass;
  
  String msg = JSON.stringify(myObject);
  Serial.println("Sending: " + msg);
  sendMessage(msg);
  
}
void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {

    switch (type) {
        case WStype_DISCONNECTED:
            Serial.printf("[WSc] Disconnected!,  restarting \n");
            webSocket.begin(ws_host, ws_port, stompUrl);
            webSocket.setAuthorization("esp82669995ROM1", "123456");
            break;
            
        case WStype_CONNECTED:
            {
                Serial.printf("[WSc] Connected to url: %s\n",  payload);
                sockets_connected = true;
                JSONVar myObject;

                myObject["channel"] = "login";
                myObject["name"] = ID;
                myObject["password"] = pass;
                
                String msg = JSON.stringify(myObject);
                Serial.println("Sending: " + msg);
                sendMessage(msg);
            }
            break;
            
        case WStype_TEXT:
            messageHandler(payload);
    }
}

void messageHandler( uint8_t *payload) {
      String text = (char*) payload;
        Serial.printf("[WSc] get text: %s\n", payload);
        JSONVar message = JSON.parse(text);
         if (JSON.typeof(message) == "undefined") {
          Serial.println("Parsing input failed!");
          return;
         }
         int type = JSON.stringify(message["type"]).toInt();
         Serial.println();Serial.println();Serial.println();
         Serial.print("type=");Serial.print(type);Serial.println("   ");

         if(type == 1){
            id = JSON.stringify(message["id"]).toInt();
            Serial.println("ID changed to "+id);
            old_data = "";
            sendData();
            return;
         }
         if(type == 2){
             saveMode(String((const char*)message["mode"]));
             Serial.println("Mode changed to "+mode);
             old_data = "";
             sendData();
             return;
         }
         if(type == 3){
            tt = JSON.stringify(message["tt"]);
            Serial.println("TT changed to "+tt);
            saveFile(timetableFileName, tt);
            old_data = "";
            sendData();
            return;
         }
}

void saveMode(String newMode) {
  
  Serial.println("newMode="+newMode);
    if( newMode.equals("wait") || newMode.equals("drain") || newMode.equals("collector") || newMode.equals("heater") ) {
      Serial.println("newMode="+newMode);
        mode = newMode;
        saveFile(modeFileName, mode);
    }
}

String getMode() {
    String newMode = getFile(modeFileName);
    if( newMode == "wait" || newMode == "drain" || newMode == "collector" || newMode == "heater" ) {
        return String(newMode);
    }
    return "wait";
}

void start_internal_server(){
        Serial.println();
        Serial.print("Configuring access point...");
        /* You can remove the password parameter if you want the AP to be open. */
        WiFi.softAP(ap_ssid, ap_password);
        IPAddress myIP = WiFi.softAPIP();
        Serial.print("AP IP address: ");
        Serial.println(myIP);

        server.on("/set_mode", handleSetMode);
        server.on("/set_temp", handleSetTemp);
        server.on("/wifi", handleConnectWifi);
       
        server.on("/", handleMain);
        server.onNotFound(handleNotFound);
        server.begin();
        Serial.println("HTTP server started");
}

String getNetworksHtml() {
    String networks;
     // WiFi.scanNetworks will return the number of networks found
    int n = WiFi.scanNetworks();
    Serial.println("scan done");
    if (n == 0) {
        Serial.println("no networks found");
    } else {
        Serial.print(n);
        networks = String("<div>") + n + String(" networks found</div>\n");
        networks += String("<div><pre class=\"prettyprint \" id=wifis>");
        Serial.println(" networks found");
        for (int i = 0; i < n; ++i){
            networks += "<span class=\"nice yellow_bg\" id=\"" + sha1(WiFi.SSID(i)) + "\"";
            networks += String(" onclick =\"connect('") + WiFi.SSID(i) + String("', '") + sha1(WiFi.SSID(i)) + String("');\">");
            Serial.print(i + 1);
            networks += i + 1;
            Serial.print(": ");
            networks += ": ";
            Serial.print(WiFi.SSID(i));
            networks += WiFi.SSID(i);
            Serial.print(" (");
            networks += " (";
            Serial.print(WiFi.RSSI(i));
            networks += WiFi.RSSI(i);
            Serial.print(")");
            networks += ")";
            Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
            networks += (WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*";
            networks += "</span><br>";
            delay(10);
        }
        networks += String("</pre></div>");
        }
        Serial.println(networks);
    return networks;
}

String getHeaderHtml(String mainClass="", String wifiClass="") {
    return "<!doctype html><html lang=\"en\"><head> <meta charset=\"UTF-8\"> <meta name=\"viewport\" content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\"> <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"> <title>****</title> <style>.pad, .w_10em{display: inline-block;}body{font-family: \"Trebuchet MS\", \"Lucida Sans Unicode\", \"Lucida Grande\", \"Lucida Sans\", Arial, sans-serif; max-width: 480px; height: 100vh;}body, button, input{font-size: large; margin: 3px;}.nice{color: #006400;}.yellow_bg:hover{background-color: #ff0; cursor: pointer;}.bubble{border: 1px solid #000; border-radius: 2px; background-color: #c7cf66; padding: 15px; max-width: 240px; position: absolute; transition: opacity 0.3s; transition: ease-in-out 0.4s;}.bubble input{max-width: 120px;}.unvisible{z-index: -1; opacity: 0;}code{font-family: monospace;}#header{margin-right: 3px;}pre{width: min-content; padding: 20px !important;}.w_10em{width: 6em; margin-right: 10px;}.pad{margin: 5px;}#main{width: fit-content;}ul{list-style-type: none; margin: 0; padding: 0; overflow: hidden; background-color: #333;}li{float: left;}li a{display: block; color: white; text-align: center; padding: 14px 16px; text-decoration: none;}.active{background-color: #c77f46;}li a:hover{background-color: #111;}</style></head><body onclick=\"hideBubble()\"><nav> <ul> <li><a href=\"/\" class=\"" + mainClass + "\">Главная</a></li><li><a href=\"/wifi\" class=\"" + wifiClass + "\">Wi-fi</a></li></ul></nav><section class=\"container\">";
}


String getWifiHtml() {
    return  getHeaderHtml("", "active")
    + "<div class=\"bubble unvisible\" id=\"bubble\"><div><label for=\"wifi_title\">Name: </label> <input id=\"wifi_title\" disabled></div>"
    + "<label for=\"wifi_pass\">Password:</label><input type=\"password\" name=\"wifi_pass\" id=\"wifi_pass\"/><br/> "
    + "<button id=\"conn_wifi\">Connect</button></div>"
    + getNetworksHtml()
    + "</section></body><script src=\"https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js\"></script><script>const baseURL=window.location.protocol + \"//\" + window.location.host + \"/\"; let x; const bubble=document.getElementById(\"bubble\"); function connect(wifiName, id){const el=document.getElementById(id); if (el.innerText.substring(0, 1)==\"*\") return; const wifiNameInput=document.getElementById(\"wifi_title\"); const savePasswordButton=document.getElementById(\"conn_wifi\"); const enteredPassword=document.getElementById(\"wifi_pass\"); enteredPassword.value=\"\"; const elClientRect=el.getBoundingClientRect(); bubble.style.top=parseInt(elClientRect.bottom)+'px'; bubble.style.left=(parseInt(elClientRect.left) + 122)+'px'; bubble.style.opacity=1; bubble.style.zIndex=5; wifiNameInput.value=wifiName.trim(); savePasswordButton.onclick=function (e){if(wifiName.length > 0 && enteredPassword.value.length > 0){window.location=baseURL + \"/?wifi_name=\" + wifiName + \"&pass=\" + enteredPassword.value;}else{alert('enter name and password');}};}function hideBubble(e){console.log('hiding bubble', event.target===document.querySelector('body')); if(event.target===document.querySelector('body')){bubble.style.opacity=0; bubble.style.zIndex=-1;}}</script></html>";
}

String getMainPageHtml() {
    JSONVar data = gatherData();

    return getHeaderHtml("active", "") + " <table class=\"table table-hover table-animate\"> "+
        "<thead><tr><th></th> <th colspan=\"2\" class=\"text-center border\">Коллектор</th> <th colspan=\"2\" class=\"text-center border\">Нагреватель</th>"+
        "<th></th> <th></th> <th></th></tr> <tr><th>Время</th> <th class=\"border-left\">t, °C</th> <th class=\"border-right\">реле</th> "+
        "<th class=\"border-left\">t, °C</th> <th class=\"border-right\">реле</th> <th class=\"border-right\">t, °C</th> <th class=\"border-right\">реле слива</th> "+
        "<th class=\"border-right\">режим</th></tr></thead><tbody> <tr> " +
        
        "<td>" + myTZ.dateTime("Y-m-d H:i:s") + "</td> <td class=\"border-left\">" + String((int)data["collector_temp"]) + "</td> <td class=\"border-right\">"
        + String((int)data["collector_relay"]) + "</td> <td class=\"border-left\">" + String((int)data["heater_temp"]) + "</td> <td class=\"border-right\">"
        + String((int)data["heater_relay"])
        + "</td> <td class=\"border-right\">" + String((int)data["inside_temp"]) + "</td> <td class=\"border-right\">" + String((int)data["drain_relay"])
        + "</td> <td class=\"border-right\">" + String((const char*)data["mode"]) + "</td></tr></tbody></table></section>"
        + "<div class=\"card\"><form method=\"post\" action=\"/set_mode\"><label>Текущий режим: <b>"
        + modeName() + "</b><select name=\"mode\">" + getOtherModesOptionsHtml()
        + "</select></label><button type=\"submit\">Сохранить</button></form></div>"
        + "</body></html>";
}

String modeName() {

     if(mode == "wait")       return "ожидание";
     if(mode == "heater")     return "обычный";
     if(mode == "collector")  return "эко";
     if(mode == "drain")      return "слив";

}

String getOtherModesOptionsHtml() {
     if(mode == "wait")       return "<option value=\"drain\">Слив</option><option value=\"collector\">Эко</option><option value=\"heater\">Обычный</option>";
     if(mode == "heater")     return "<option value=\"drain\">Слив</option><option value=\"wait\">Остановлен</option><option value=\"collector\">Эко</option>";
     if(mode == "collector")  return "<option value=\"drain\">Слив</option><option value=\"wait\">Остановлен</option><option value=\"heater\">Обычный</option>";
     if(mode == "drain")      return "<option value=\"collector\">Эко</option><option value=\"wait\">Остановлен</option><option value=\"heater\">Обычный</option>";
}

void handleConnectWifi() {
  server.send(200, "text/html", getWifiHtml());
}

void handleMain() {
  server.send(200, "text/html", getMainPageHtml());
}

void handleSetMode() {
  if (server.method() != HTTP_POST) {
    server.send(405, "text/plain", "Method Not Allowed");
  } else {
    if(server.argName(0) == "mode" && (server.arg(0) == "wait" || server.arg(0) == "drain" || server.arg(0) == "collector" || server.arg(0) == "heater" )) {
        Serial.println('setting mode to '+ server.arg(0));
        mode = server.arg(0);
        server.sendHeader("Location", String("/"), true);
        server.send ( 302, "text/plain", "ok");
//        server.send(200, "application/json", "{\"status\":true, \"message\":\"mode changed to "+server.arg(0) + "\"}");
    }
    server.send(404, "application/json", "{\"status\":false, \"message\":\"mode not recognised\"}");
  }
}

void handleSetTemp() {
 if (server.method() != HTTP_POST) {
    server.send(405, "text/plain", "Method Not Allowed");
  } else {
    if(server.argName(0) == "temp" && server.arg(0).toInt() > 0 && server.arg(0).toInt() < 75 ) {
        httpTemp = server.arg(0).toInt();
        server.send(200, "application/json", "{\"status\":true, \"message\":\"temp changed to "+server.arg(0) + "\"}");
    }
    server.send(404, "application/json", "{\"status\":false, \"message\":\"temperature not recognised\"}");
  }
}

void handleNotFound() {
  if (server.uri().equals("/favicon.ico")) return;
  String wifi_name = "";
  String pass = "";
  String message;
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    if (server.argName(i).equals(String("wifi_name"))) {
      Serial.println("found wifi_name" + server.arg(i));
      wifi_name = server.arg(i);
    }
    if (server.argName(i).equals(String("pass"))) wifi_name = server.arg(i);
  }
  Serial.println(message);

  Serial.println("saving ssid " + server.arg(0));
  Serial.println("saving pass " + server.arg(1));
  save_pass(String(server.arg(1)));
  save_ssid(String(server.arg(0)));
  Serial.println("");
  Serial.println(getFile(ssidFileName));
  Serial.println("");
  Serial.println(getFile(passFileName));

  server.send(200, "text/html", script + getNetworksHtml());
}

void save_ssid(String ssid) {
  File file = SPIFFS.open(ssidFileName, "w");
  if (!file) {
    Serial.println("file open failed");
  } else {
    Serial.println(ssid + " wifi ");
    file.println(ssid);
    file.close();
  }
}

char* get_pass() {
    String savedPass = getFile(passFileName);
    if(savedPass != "") {
        return (char*) savedPass.c_str();
    } else {
        return (char*) wlan_password;
    }
}


char* get_ssid() {
    String savedSsid = getFile(ssidFileName);
    if(savedSsid != "") {
        return (char*) savedSsid.c_str();
    } else {
        return (char*) wlan_ssid;
    }
}

void save_pass(String pass) {
  saveFile(passFileName, pass);
}

bool saveFile(String file_name, String data) {
  File file = SPIFFS.open(file_name, "w");
  if (!file) {
    Serial.println("file open failed..formatting");
    SPIFFS.format();
    delay(30000);
    saveFile(file_name, data);
  } else {
    file.println(data);
    file.close();
  }
  String result = getFile(file_name);
//  Serial.println(result);
  return result.equals(data);
}

String getFile(String file_name) {
  String pass;
  File file = SPIFFS.open(file_name, "r");
  if (!file) {
    Serial.println("file open failed");
    return "";
  } else {
    while (file.available()) {
      pass += char(file.read());
    }
    file.close();
  }
  pass.trim();

  return pass;
}

void parseTimetable() {

  if(httpTemp >0 && httpTemp <= 75 ) {
    target_heater_temp = httpTemp;
    Serial.println("Using Emergency temp setting, target_heater_temp = "+ httpTemp);
    return;
  }
  
      float hour = myTZ.hour();
      hour +=int(int(myTZ.minute()/6.0*10.0)   /5.0)/20.0;
//      Serial.print("hour=");Serial.println(hour);
      int maxtemp = 20;
      String fileContents = getFile(timetableFileName);
      JSONVar data = JSON.parse(fileContents);

//      if(!data.length()){
//        return;
//      }
//
//      Serial.println(String(timetableFileName) + " file contents: ");
//      Serial.println(fileContents);


  // JSON.typeof(jsonVar) can be used to get the type of the var
  if (JSON.typeof(data) == "undefined") {
    Serial.println("Parsing input failed!");
    return;
  }

      timetable* tts;
      timetable tmp;
     
      for (int i=0; i < data.length(); i++) {
          JSONVar line = data[i];
//          Serial.println(line);
          tmp.what = (int)(line["what"]);
          tmp.days_of_week = parse_days_of_week((int)(line["days_of_week"]));
          String from_time = String((const char*)line["from_time"]);
          String h = from_time.substring(0, 2); 

          int from_hours   = int(h.toInt());
          int from_minutes = int(from_time.substring(3, 5).toInt());
          float from_minutes_percent = (int(int(from_minutes / 6.0 * 10.0)   / 5.0)) / 20.0;
//          Serial.print("fh="+from_time+", ");Serial.print(h.toInt());Serial.print(".");Serial.println(from_minutes_percent);
          tmp.from_time = from_hours + from_minutes_percent;
          String till_time = String((const char*)data[i]["till_time"]);
          int till_hours   = int(till_time.substring(0, 2).toInt());
          int till_minutes = int(till_time.substring(3, 5).toInt());
          float till_minutes_percent = (int(int(till_minutes / 6.0 * 10.0) / 5.0)) / 20.0;

          tmp.till_time = till_hours + till_minutes_percent;
          tmp.temp = String((const char*)data[i]["temp"]).toInt();
//          Serial.print("    from_time=");
//          Serial.print((tmp.from_time));
//          Serial.println(" " + from_time);

//          Serial.print("    till_time=");
//          Serial.print((tmp.till_time));
//          Serial.println(" " + till_time);

//          Serial.println("    temp=" + String(tmp.temp));
//          Serial.println(tmp.days_of_week);
//          Serial.println(myTZ.dateTime("D")+" "+tmp.days_of_week.indexOf(myTZ.dateTime("D")));
          if (tmp.days_of_week.indexOf(myTZ.dateTime("D")) > 0) {
//            Serial.println("In tt!!! ");
            if(tmp.from_time<tmp.till_time){
//                Serial.print("hour=");
//                Serial.println(hour);
              if(hour >= tmp.from_time && hour < tmp.till_time){
                maxtemp = tmp.temp;
//                Serial.println(maxtemp+" day mode          (get_max_temp)\n");
              }
            }else{
              if(hour >= tmp.from_time || hour < tmp.till_time){
                maxtemp = tmp.temp;
//              Serial.println(maxtemp+" night mode         (get_max_temp)\n");
              }
            }
          }
       
      }
      Serial.print("maxtemp=");Serial.println(maxtemp);
      target_heater_temp = maxtemp;
 }


String parse_days_of_week(byte dw) {
  String val = String(dw, BIN);
  if(val.length()<7){
    String nulls="";
    for(int i=0;i<7-val.length();i++){
      nulls+="0";
    }
    val=nulls+val;
  }

 String days[7] = {" Mon ", " Tue ", " Wed ", " Thu ", " Fri ", " Sat ", " Sun "};
  String arr = "";
  for(int k=0;k<=6;k++){
      if ((val[k]) == ('1'))arr += days[k];
  }
  return arr;
}

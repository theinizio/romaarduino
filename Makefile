include .env


up: # create and start containers
	@docker-compose -f ${DOCKER_CONFIG} up -d

down: # stop and destroy containers
	@docker-compose -f ${DOCKER_CONFIG} down

down-volume: #  WARNING: stop and destroy containers with volumes
	@docker-compose -f ${DOCKER_CONFIG} down -v

start: # start already created containers
	@docker-compose -f ${DOCKER_CONFIG} start

stop: # stop containers, but not destroy
	@docker-compose -f ${DOCKER_CONFIG} stop

ps: # show started containers and their status
	@docker-compose -f ${DOCKER_CONFIG} ps

build: # build all dockerfile, if not built yet
	@docker-compose -f ${DOCKER_CONFIG} build

npm_run: # npm run dev
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www/frontend node npm run node

build_front: # npm run build
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www/frontend node npm run build


connect_app: # app command line
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www/laravel app sh

connect_app_root: # app command line
	@docker-compose -f ${DOCKER_CONFIG} exec -w /www/laravel app sh

connect_adonis: # node command line
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www/adonis adonis sh

connect_sockets: # node command line
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www/sockets socket sh

connect_frontend: # node command line
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www/frontend frontend sh

connect_adonis_root: # node command line from root
	@docker-compose -f ${DOCKER_CONFIG} exec -w /www/ adonis sh

connect_socket_root: # node command line from root
	@docker-compose -f ${DOCKER_CONFIG} exec -w /www/ socket sh

connect_nginx: # nginx command line
	@docker-compose -f ${DOCKER_CONFIG} exec -w /www nginx sh

connect_db: # database command line
	@docker-compose -f ${DOCKER_CONFIG} exec db bash


database-open:
	@docker-compose -f ${DOCKER_CONFIG} exec db mysql -uroot -p${DOCKER_PASSWORD} ${DOCKER_DATABASE}

database-dump: # dump database
	@docker-compose -f ${DOCKER_CONFIG} exec db bash -c "mysqldump ${DOCKER_DATABASE} -u${DOCKER_USERNAME} -p${DOCKER_PASSWORD} 2> /dev/null"


node_Modules: # npm install
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www/sockets adonis npm install
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www/adonis adonis npm install
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www/frontend adonis yarn install

fresh:  # refresh the database and run all database seeds
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www/adonis adonis adonis migration:refresh --seed


swagger: # generate swagger doc
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www/laravel app  php artisan l5-swagger:generate


test: # run all tests
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www/laravel app php vendor/bin/phpunit

(async () => {

    const TYPE_SEND_ID = 1;
    const TYPE_SEND_MODE = 2;
    const TYPE_SEND_TIMETABLE = 3;
    const express = require("express")
    const expressWs = require("express-ws")
    const WebSocket = require("ws")
    const {createServer} = require("http")

    const mysql = require('mysql2/promise')
    const pool = mysql.createPool({
        host: 'db',
        user: 'db_user',
        database: 'db_name',
        password: 'db_pass',
    })


        let connectedDevices = []

     function send(data, deviceId) {

        const device = findDeviceById(deviceId) || findDeviceByName(deviceId)
        console.log(`sending: ${JSON.stringify(data)} to device ${deviceId} ${!!device}`)
        if(! device || !device.socket){
            return "device not found"
        }
        device.socket.send(JSON.stringify(data))
        return 'ok'
    }

    /**
     * Searches the currently connected devices and returns the first one connected to the provided socket.
     * @param socket The socket to search for
     */
    function findDeviceBySocket(socket){
        return connectedDevices.find((device) => device.socket === socket)
    }

    /**
     * Searches the currently connected devices and returns the first one with the provided id.
     * @param id
     */
    function findDeviceById(id) {
        return connectedDevices.find((device) => device.id === parseInt(id))
    }

    /**
     * Searches the currently connected devices and returns the first one with the provided name.
     * @param name
     */
    function findDeviceByName(name) {
        return connectedDevices.find((device) => device.name === name)
    }


    /**
     * Processes the incoming message.
     * @param socket The socket that sent the message
     * @param message The message itself
     */
    async function handleMessage(socket, message) {

        switch (message.channel) {
            case "login":
                try {
                    const result= await findOrCreateDevice(message.name, message.password)
                    const device = {socket, name: message.name, ...result}
                    connectedDevices.push(device)
                    send({type: TYPE_SEND_ID, id: result.id}, device.id)
                    const tt= await getLastDevicesTimetable(device.id);
                    send({type: TYPE_SEND_TIMETABLE, tt: tt}, device.id)
                } catch (e) {
                    console.log(e)
                }
                break

            case "data":
                saveData(message)
                break

            case "error":
                handleError(message)
                break

            default:
                console.log("unknown message", message)
                break
        }
    }

    async function saveData(message) {
        try {
            const {id, device_time, collector_temp, heater_temp, inside_temp, collector_relay, heater_relay, drain_relay, mode} = message
            return await pool.query(
                    `insert into data (device_id, device_time, collector_temp, heater_temp, inside_temp,
                                       collector_relay,
                                       heater_relay, drain_relay, mode)
                     values (?, ?, ?, ?, ?, ?, ?, ?, ?)`,
                [id, device_time, collector_temp, heater_temp, inside_temp, collector_relay, heater_relay, drain_relay, mode]
            )
        }catch (e) {
            console.log(e)
        }
    }

    async function getLastDevicesTimetable(deviceId) {
        try {
            const [rows, fields] =  await pool.query(
                'select * from  timetables where device_id in (?,0) ',
                [deviceId]
            )
            return rows
        } catch (e) {
            console.error(e)
        }
    }
    async function getLastDevicesMode(deviceId) {
        try {
            const [rows, fields] =  await pool.query(
                'select * from  timetables where device_id in (?,0) ',
                [deviceId]
            )
            return rows
        } catch (e) {
            console.error(e)
        }
    }

    async function handleError(message) {
        try {
            const {id, error} = message
            return await pool.query(
                'insert into errors (device_id, description) values (?, ?)',
                [id, error],
            )
        }catch (e) {
            console.log(e)
        }
    }

    async function findOrCreateDevice(name, password) {
        const [[result]] = await pool.query(
            'select * from devices where name=? and password = ?', [name, password])
        if(result){
            return result
        }
        const insertResult =  await pool.query(
            `insert into devices (name, password, created_at) VALUE (?,?, ?)`, [name, password, new Date])
        return {id:insertResult.insertId, name: name, password:password, created_at: new Date()}
    }

    /**
     * Adds event listeners to the incoming socket.
     * @param socket The incoming WebSocket
     */
    function handleSocketConnection(socket) {
        socket.addEventListener("message", (event) => {
            try {
                let result = event.data.toString()
                result = result.replace('\u0000', '').split('').join('')
                const json = JSON.parse(result)
                handleMessage(socket, json)
            } catch (e) {
                console.log(e)
            }
        })

        socket.addEventListener("close", () => {
            connectedDevices = connectedDevices.filter((device) => {
                return device.socket !== socket;

            })
        })
    }

     function handleSendMode(req, res) {
        try {
            res.send(send({type: TYPE_SEND_MODE, mode: req.body.mode}, req.body.device_id))
        }catch (e) {
            console.dir(e)
        }
    }

     function handleSendTt(req, res) {
        try {
            res.send(send({type: TYPE_SEND_TIMETABLE, tt: req.body.tt}, req.body.device_id))
        }catch (e) {
            console.dir(e)
        }
    }

    function handleIsDeviceOnline(req, res) {
        try {
            if(!connectedDevices.length) {
                return res.send(false);
            }
            if(req.query.device_id) {
                const result = connectedDevices.find(device => device.id === parseInt(req.query.device_id));
                console.log('one device', result?result.name || '':"" );
                res.send( !!result)
            }

            if(req.query.devices_id) {
                res.send(req.query.devices_id.split(',').map(id => {
                    const result = connectedDevices.find(device => device.id === parseInt(id))
                    console.log(result?result.name || '':"");
                    return !! result;
                }))
            }

        }catch (e) {
            console.dir(e)
        }
    }


// create an express app, using http `createServer`
    const app = express()
    const server = createServer(app)
    app.use(express.json())
    app.use(express.urlencoded({ extended: true }))

    app.post('/device/send_mode',   handleSendMode)
    app.post('/device/send_tt', handleSendTt)
    app.get('/device/is_online', handleIsDeviceOnline)

// add a websocket listener under /websock
    const wsApp = expressWs(app, server).app
    wsApp.ws("/websock", handleSocketConnection)

    const port = 3000
    server.listen(port, () => {
        console.log(`server started on http://localhost:${port}`)
    })

})()